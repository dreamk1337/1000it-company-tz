@extends('layouts.control_new')

<?php
/**
 * @var $types array
 */
?>

@section('content_head')
    <div class="container">
        <div class="row content-center">
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        <strong>Добавить рубрику</strong>
                    </div>
                </div>
            </div>
        </div>
        @include('categories._partarials._form')
    </div>
@endsection
