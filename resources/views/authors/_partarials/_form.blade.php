
<form method="POST" action="{{route('authors.store')}}">
    @csrf
    <div>
        <label>{{trans('ФИО автора')}}
            <input name="fio">
            {!! $errors->first('fio','<span >:message</span>') !!}
        </label>
    </div>
    <div>
        <label>{{trans('email')}}
            <input name="email">
            {!! $errors->first('email','<span >:message</span>') !!}
        </label>
    </div>
    <button type="submit">Добавить</button>
</form>
