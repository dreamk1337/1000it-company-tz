<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;


/**
 *
* @mixin \Eloquent
 * @property int                $id
 * @property string             $title
 * @property string             $announcement
 * @property string             $text
 * @property Carbon             $date_publicate
 * @property string             $author
 * @property string             $category
 * @property Carbon             $created_at
 * @property Carbon             $updated_at
 * @property-read category|null                          $category
 * @property-read author|null                            $author
 */
class News extends Model
{

    protected $table = 'news';

    protected $fillable = [
        'title',
        'announcement',
        'text',
        'date_publicate',
        'author',
        'category',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category', 'id');
    }

    public function author()
    {
        return $this->belongsTo(Author::class, 'author', 'id');
    }

}
