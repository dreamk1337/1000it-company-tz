<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoriesRequest;
use App\Models\Category;

class CategoriesController extends Controller
{
    public function create()
    {
        return view(
            'categories.create'
        );
    }

    public function store(CategoriesRequest $request)
    {
        $data = $request->all();
        Category::create($data);
    }
}
