<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewsRequest;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function create()
    {
        return view(
            'news.create'
        );
    }

    public function store(NewsRequest $request)
    {
        $data = $request->all();
        News::create($data);
    }
}
