<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthorsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        $rules = [
            'fio' => 'required|string',
            'email' => 'required|string',
        ];
        return $rules;
    }

    public function attributes()
    {
        return [
            'fio'  => trans('ФИО автора'),
            'email'  => trans('почта эмайл'),
        ];
    }
}
