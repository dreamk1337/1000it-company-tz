<?php

namespace App\Http\Resources;

use App\Http\Resources\Json\BaseAnonymousResourceCollection;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;

/**
 * Class BaseResource
 * @package App\Http\Resources
 */
class BaseResource extends Resource
{
    /**
     * Create new anonymous resource collection.
     *
     * @param mixed $resource
     * @return BaseAnonymousResourceCollection
     */
    public static function collection($resource)
    {
        return tap(
            new BaseAnonymousResourceCollection($resource, static::class),
            function ($collection) {
                if (property_exists(static::class, 'preserveKeys')) {
                    $collection->preserveKeys = (new static([]))->preserveKeys === true;
                }
            }
        );
    }

    public static function makeFakePaginator($items)
    {
        return new LengthAwarePaginator(
            Arr::get($items, 'data'),
            Arr::get($items, 'meta.total'),
            Arr::get($items, 'meta.per_page'),
            Arr::get($items, 'meta.current_page'),
            [
                'path' => Arr::get($items, 'meta.path'),
                'pageName' => 'page',
            ]
        );
    }
}
